const listContainer =  document.getElementById('listing')
const tasksContainer =  document.getElementById('listingTask')
const listForm = document.querySelector('#listForm')
const listFormName = document.querySelector('#listFormData')
const currentList = document.getElementById('currentList')
const templateTask = document.getElementById('templateTask')

const taskForm = document.querySelector('#taskForm')
const taskFormName = document.querySelector('#taskFormData')

const deleteButton = document.getElementsByTagName('button')
const postItContainer  = document.querySelector('#blockPostIt')


// const LOCAL_STORAGE_LIST_KEY = 'task.lists'
// const LOCAL_STORAGE_SELECTED_LIST_ID_KEY = 'task.selectedListId'
// let lists = JSON.parse(localStorage.getItem(LOCAL_STORAGE_LIST_KEY)) || []
// let selectedListId = localStorage.getItem(LOCAL_STORAGE_SELECTED_LIST_ID_KEY)
let lists = []
let selectedListId



listContainer.addEventListener('click', e => {
    if (e.target.tagName.toLowerCase() === 'li') {
        selectedListId = e.target.id

        render()
    }
})

listForm.addEventListener('submit', e => {
    e.preventDefault()

    const listName = listFormName.value
    if (listName === '' || listName== null ) return
    const list = createList(listName)
    listFormName.value =null
    lists.push(list)
    selectedListId = list.id

    render()
})

taskForm.addEventListener('submit', e => {
    e.preventDefault()


    const taskName = taskFormName.value
    if (taskName == null || taskName === '') return
    const task = createTask(taskName)
    taskFormName.value = null
    const selectedList = lists.find(list => list.id === selectedListId)

    selectedList.tasks.push(task)
    render()
})



function createList(l) {
    return {
        name : l,
        id : Date.now().toString(),
        tasks : []
    }

}
function createTask(name) {
    return { id: Date.now().toString(), name: name, complete: false }
}
function updateCheckbox(selectedList,taskId) {
    const TasksToUpdate= selectedList.tasks.filter(task => task.id === taskId)
    TasksToUpdate[0].complete != true ?  TasksToUpdate[0].complete = true : TasksToUpdate[0].complete = false


}
function removeTask(selectedList,taskId) {
    const  TasksToKeep= selectedList.tasks.filter(task => task.id !== taskId)
    selectedList.tasks = TasksToKeep
}
function removeList(id) {
    const ListsToKeep= lists.filter(list => list.id !== id)
    lists = ListsToKeep
    // selectedList.tasks = TasksToKeep

}
function removeElement(el) {
    while (el.firstChild != null){
        el.removeChild(el.firstChild)

    }

}
function renderLists() {
    lists.forEach(list => {

        const listElement = document.importNode(templateTask.content, true)
        const buttonDeleteList = listElement.getElementById("deleteTask")
        const listElem = document.createElement('li')
        listElem.innerText = list.name
        listElem.setAttribute('id',list.id)
        listElem.appendChild(buttonDeleteList)
        listContainer.appendChild(listElem)
        buttonDeleteList.addEventListener('click', function () {
            removeList(list.id)
            render()

        })

    })
}
function renderTasks(selectedList) {
    selectedList.tasks.forEach(task => {
        const taskElement = document.importNode(templateTask.content, true)
        const label = taskElement.querySelector('label')
        const buttonDelete = taskElement.getElementById("deleteTask")
        const checkbox = taskElement.querySelector('input')

        checkbox.checked = task.complete
        if (task.complete) checkbox.checked = true
        label.innerText = task.name
        const taskElem = document.createElement('li')

        taskElem.id = task.id
        taskElem.appendChild(checkbox)
        taskElem.appendChild(label)
        taskElem.appendChild(buttonDelete)
        tasksContainer.appendChild(taskElem)
        buttonDelete.addEventListener('click', function () {

            removeTask(selectedList, this.parentNode.id)
            render()

        })
        checkbox.addEventListener('change', function() {
            updateCheckbox(selectedList, this.parentNode.id)
            render()
        })

    })

}

function render() {

    removeElement(listContainer)
    removeElement(tasksContainer)
    renderLists()
    const selectedList = lists.find(list => list.id === selectedListId)
    if (selectedList == null) {
        postItContainer.style.display = 'none'

    } else {
        postItContainer.style.display = ''

        const nb_tasks = selectedList.tasks.length
        const nb_tasks_done  = selectedList.tasks.filter(task => task.complete === true).length
        currentList.innerText = `Liste: ${selectedList.name} \n nombre de taches: ${nb_tasks_done}/${nb_tasks!= null ? nb_tasks : 0 }`
        renderTasks(selectedList)
    }

}


render()
